'''
Created on Oct 30, 2013

@author: bhanu
'''
import socket
import os
computerName = socket.gethostname()
PROJECT_HOME = '/home/gaurav/workspace/mv-rnn'
parsed_file =           PROJECT_HOME+'/iais/data/corpus/parsed.txt' 
cw_embeddings_file =    PROJECT_HOME+'/iais/data/corpus/CW_embedddings.mat' 
train_data =            PROJECT_HOME+'/iais/data/corpus/allDataTrain.mat' 
pre_trained_weights =   PROJECT_HOME+'/iais/data/corpus/pretrainedWeights.mat' 
saved_params_file =     PROJECT_HOME+'/iais/data/models/tuned_params'
test_data  =            PROJECT_HOME+'/iais/data/corpus/allDataTest.mat' 
test_labels_output =    PROJECT_HOME+'/iais/data/results/test_predictions.txt' 
results_path        =   PROJECT_HOME+'/iais/data/results/'
semeval_testKeys    =   PROJECT_HOME+'/iais/data/results/TEST_FILE_KEY.TXT'
test_data_srl       =   PROJECT_HOME+'/iais/data/corpus/FinalTestData_srl2.mat'
dev_data_srl       =   PROJECT_HOME+'/iais/data/corpus/FinalDevData_srl2.mat'
train_data_srl       =   PROJECT_HOME+'/iais/data/corpus/FinalTrainData_srl2.mat' 
model_path          =   PROJECT_HOME+'/iais/data/models/'
corpus_path         =   PROJECT_HOME+'/iais/data/corpus/'
