# Matrix-Vector Recursive Neural Network #

Complete guide for how to setup and test this model would be up as soon as coding completes.


## What is this repository for? ##

### Quick Summary ###

This model would let you extract semantic relation between the various entities in the data-set. By saying that, what I meant is - given a raw text file, it would extract the entities along with the semantic relations between them which are meaningful to humans.


## How do I get set up? ##

* Summary of set up
* Configuration
* Dependencies
* Matlab
* Python Anaconda

* How to run tests
* Deployment instructions


### Who do I talk to? ###

* Gaurav Agarwal (gaurav.agarwal@students.iiit.ac.in)
* Sapan Sah (sapan.hs@tcs.com)
